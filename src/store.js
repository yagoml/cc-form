import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  actions: {
    async pay({ commit }, data) {
      const route = 'api/pagar'

      let config = {
        method: 'post',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }

      fetch(route, config).then(res => {
        return res.json()
      })
    }
  }
})